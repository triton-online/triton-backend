package online.triton.tritonbackend.constants

const val MAX_POST_TEXT_LENGTH = 1000

const val PARAMETER_NOT_VALID = "PARAMETER_NOT_VALID"
