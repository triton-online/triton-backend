package online.triton.tritonbackend.services

import online.triton.tritonbackend.dto.CreateUpdatePostDTO
import online.triton.tritonbackend.dto.PostOutDTO
import online.triton.tritonbackend.dto.PostsPageDTO

interface PostService {
    fun getPage(pageIndex: Int, pageSize: Int): PostsPageDTO
    fun getById(id: String): PostOutDTO
    fun create(post: CreateUpdatePostDTO): PostOutDTO
    fun delete(id: String): PostOutDTO
}
