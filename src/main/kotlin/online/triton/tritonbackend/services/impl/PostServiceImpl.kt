package online.triton.tritonbackend.services.impl

import online.triton.tritonbackend.constants.MAX_POST_TEXT_LENGTH
import online.triton.tritonbackend.dto.CreateUpdatePostDTO
import online.triton.tritonbackend.dto.PostOutDTO
import online.triton.tritonbackend.dto.PostsPageDTO
import online.triton.tritonbackend.exceptions.PostNotFoundException
import online.triton.tritonbackend.exceptions.PostTextIsTooLongException
import online.triton.tritonbackend.models.PostModel
import online.triton.tritonbackend.repositories.PostRepository
import online.triton.tritonbackend.services.PostService
import online.triton.tritonbackend.utils.mapToModel
import online.triton.tritonbackend.utils.mapToOutDTO
import org.bson.types.ObjectId
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class PostServiceImpl(private val postRepo: PostRepository) : PostService {
    override fun getPage(pageIndex: Int, pageSize: Int): PostsPageDTO {
        val pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by(Sort.Direction.DESC, "createdAt"))
        val page = postRepo.findAll(pageRequest)
        return PostsPageDTO(page.toList().map { it.mapToOutDTO() }, page.totalPages)
    }

    override fun getById(id: String): PostOutDTO {
        return postRepo.findByIdOrNull(id)?.mapToOutDTO() ?: throw PostNotFoundException(id)
    }

    override fun create(post: CreateUpdatePostDTO): PostOutDTO {
        if (post.text.length > MAX_POST_TEXT_LENGTH) {
            throw PostTextIsTooLongException(post.text)
        }

        return postRepo.save(post.mapToModel()).mapToOutDTO()
    }

    override fun delete(id: String): PostOutDTO {
        val post = postRepo.findByIdOrNull(id) ?: throw PostNotFoundException(id)
        postRepo.delete(post)
        return post.mapToOutDTO()
    }
}
