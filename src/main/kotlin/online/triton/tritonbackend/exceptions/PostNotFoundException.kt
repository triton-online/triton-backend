package online.triton.tritonbackend.exceptions

import org.springframework.http.HttpStatus

class PostNotFoundException(val id: String) : BaseAPIException(
    "Post with ID = $id not found",
    ERROR_CODE,
    HttpStatus.NOT_FOUND,
) {
    companion object {
        const val ERROR_CODE = "POST_NOT_FOUND"
    }
}
