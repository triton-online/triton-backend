package online.triton.tritonbackend.exceptions

import org.springframework.http.HttpStatus

class PostTextIsTooLongException(val text: String) : BaseAPIException(
    "Post text is too long",
    ERROR_CODE,
    HttpStatus.BAD_REQUEST
) {
    companion object {
        const val ERROR_CODE = "POST_TEXT_IS_TOO_LONG"
    }
}
