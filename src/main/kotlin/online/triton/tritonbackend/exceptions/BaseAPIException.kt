package online.triton.tritonbackend.exceptions

import org.springframework.http.HttpStatus

abstract class BaseAPIException(
    message: String,
    val errorCode: String,
    val httpStatus: HttpStatus
) : RuntimeException(message)
