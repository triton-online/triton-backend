package online.triton.tritonbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TritonBackendApplication

fun main(args: Array<String>) {
    runApplication<TritonBackendApplication>(*args)
}
