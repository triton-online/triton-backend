package online.triton.tritonbackend.models

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document("Posts")
data class PostModel(
    var id: String = ObjectId.get().toString(),
    val createdAt: Instant = Instant.now(),
    var text: String,
)
