package online.triton.tritonbackend.controllers

import online.triton.tritonbackend.dto.CreateUpdatePostDTO
import online.triton.tritonbackend.services.PostService
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.constraints.Min

@Validated
@RestController
@RequestMapping("posts")
class PostController(private val postService: PostService) {
    @GetMapping
    fun getPage(
        @RequestParam(defaultValue = "0") @Min(0) pageIndex: Int,
        @RequestParam(defaultValue = "10") @Min(1) pageSize: Int,
    ) = postService.getPage(pageIndex, pageSize)

    @GetMapping("{id}")
    fun getById(@PathVariable id: String) = postService.getById(id)

    @PostMapping
    fun create(@RequestBody post: CreateUpdatePostDTO) = postService.create(post)

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: String) = postService.delete(id)
}
