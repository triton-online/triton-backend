package online.triton.tritonbackend.controllers

import online.triton.tritonbackend.constants.PARAMETER_NOT_VALID
import online.triton.tritonbackend.dto.APIErrorBody
import online.triton.tritonbackend.exceptions.BaseAPIException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.validation.ConstraintViolationException

@ControllerAdvice
class ExceptionHandler {
    @ExceptionHandler(BaseAPIException::class)
    fun handleBaseAPIException(exception: BaseAPIException): ResponseEntity<APIErrorBody> {
        return ResponseEntity(APIErrorBody(exception.errorCode, exception.message), exception.httpStatus)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    fun handleConstraintViolationException(exception: ConstraintViolationException): ResponseEntity<APIErrorBody> {
        return ResponseEntity(APIErrorBody(PARAMETER_NOT_VALID, exception.message), HttpStatus.BAD_REQUEST)
    }
}
