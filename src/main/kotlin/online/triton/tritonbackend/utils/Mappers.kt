package online.triton.tritonbackend.utils

import online.triton.tritonbackend.dto.CreateUpdatePostDTO
import online.triton.tritonbackend.dto.PostOutDTO
import online.triton.tritonbackend.models.PostModel

fun PostModel.mapToOutDTO() = PostOutDTO(id, createdAt, text)

fun CreateUpdatePostDTO.mapToModel() = PostModel(text = text)
