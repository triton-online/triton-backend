package online.triton.tritonbackend.repositories

import online.triton.tritonbackend.models.PostModel
import org.bson.types.ObjectId
import org.springframework.data.repository.PagingAndSortingRepository

interface PostRepository : PagingAndSortingRepository<PostModel, String>
