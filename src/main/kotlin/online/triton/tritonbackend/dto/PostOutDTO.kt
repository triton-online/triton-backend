package online.triton.tritonbackend.dto

import java.time.Instant

data class PostOutDTO(
    val id: String,
    val createdAt: Instant,
    val text: String,
)
