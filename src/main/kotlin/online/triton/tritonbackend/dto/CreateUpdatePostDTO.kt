package online.triton.tritonbackend.dto

data class CreateUpdatePostDTO(
    val text: String,
)
