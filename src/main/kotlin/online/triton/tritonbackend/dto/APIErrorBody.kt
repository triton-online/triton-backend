package online.triton.tritonbackend.dto

data class APIErrorBody(
    val errorCode: String,
    val description: String?,
)