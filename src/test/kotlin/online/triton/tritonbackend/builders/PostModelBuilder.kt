package online.triton.tritonbackend.builders

import online.triton.tritonbackend.models.PostModel
import online.triton.tritonbackend.utils.generateRandomTextWithLength
import java.time.Instant

fun posts(configure: PostModelBuilder.() -> Unit): List<PostModel> {
    val builder = PostModelBuilder()
    builder.configure()
    return builder.build()
}

class PostModelThatElement : ThatElement() {
    var createdAt: Instant = Instant.now()
        private set

    var text: String = ""
        private set

    fun hasText(text: String) {
        this.text = text
    }

    fun hasRandomTextWithLength(length: Int) {
        this.text = generateRandomTextWithLength(length)
    }

    fun createdAt(date: Instant) {
        createdAt = date
    }
}

class PostModelBuilder : TestDataBuilder<PostModelThatElement, PostModel>({
    createThatElement = { PostModelThatElement() }
    mapThatElementToObject = { PostModel(createdAt = it.createdAt, text = it.text) }
})
