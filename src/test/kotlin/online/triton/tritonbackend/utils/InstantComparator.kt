package online.triton.tritonbackend.utils

import java.time.Instant
import java.time.temporal.ChronoUnit

class InstantComparator : Comparator<Instant> {
    override fun compare(p0: Instant, p1: Instant): Int {
        return p0.truncatedTo(ChronoUnit.SECONDS).compareTo(p1.truncatedTo(ChronoUnit.SECONDS))
    }
}
