package online.triton.tritonbackend.integration

import online.triton.tritonbackend.builders.posts
import online.triton.tritonbackend.constants.MAX_POST_TEXT_LENGTH
import online.triton.tritonbackend.dto.CreateUpdatePostDTO
import online.triton.tritonbackend.exceptions.PostNotFoundException
import online.triton.tritonbackend.exceptions.PostTextIsTooLongException
import online.triton.tritonbackend.repositories.PostRepository
import online.triton.tritonbackend.services.PostService
import online.triton.tritonbackend.utils.InstantComparator
import online.triton.tritonbackend.utils.generateRandomTextWithLength
import online.triton.tritonbackend.utils.mapToOutDTO
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.bson.types.ObjectId
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.repository.findByIdOrNull
import java.time.Duration
import java.time.Instant
import java.time.Period
import java.time.temporal.ChronoUnit

@SpringBootTest
@AutoConfigureDataMongo
class PostServiceTest {
    @Autowired
    private lateinit var postService: PostService

    @Autowired
    private lateinit var postRepository: PostRepository

    @BeforeEach
    fun beforeEach() {
        postRepository.deleteAll()
    }

    @Test
    fun `test pagination in getPage`() {
        // Arrange
        val baseDate = Instant.now()

        val posts = posts {
            (10L until 20L).reversed().forEach {
                that {
                    hasText("page 0")
                    createdAt(baseDate + Duration.ofDays(it))
                }
            }
            (0L until 10L).reversed().forEach {
                that {
                    hasText("page 1")
                    createdAt(baseDate + Duration.ofDays(it))
                }
            }
            that {
                hasCount(100)
                createdAt(baseDate - Duration.ofDays(1))
            }
        }
        postRepository.saveAll(posts)

        // Act
        val page0 = postService.getPage(0, 10)
        val page1 = postService.getPage(1, 10)
        val emptyPage = postService.getPage(100, 10)

        // Assert
        assertThat(page0.posts).hasSize(10)
        assertThat(page1.posts).hasSize(10)
        assertThat(emptyPage.posts).hasSize(0)

        assertThat(page0.posts).allMatch { it.text == "page 0" }
        assertThat(page1.posts).allMatch { it.text == "page 1" }

        assertThat(page0.totalPages).isEqualTo(12)
    }

    @Test
    fun `test sorting by createdAt in getPage`() {
        // Arrange
        val baseDate = Instant.now()
        val dates = listOf(3, 5, 1, 7, 6, 10, 2, 8).map { baseDate + Period.ofDays(it) }
        val dateOfFirstPostOtSecondPage = baseDate + Period.ofDays(5)

        val posts = posts {
            dates.forEach { date ->
                that { createdAt(date) }
            }
        }
        postRepository.saveAll(posts)

        // Act
        val page = postService.getPage(1, 4)

        // Assert
        assertThat(page.posts.map { it.createdAt }).isSortedAccordingTo(Comparator.reverseOrder())
        assertThat(page.posts[0].createdAt).usingComparator(InstantComparator()).isEqualTo(dateOfFirstPostOtSecondPage)
    }

    @Test
    fun `test getById`() {
        // Arrange
        val posts = posts {
            that { hasCount(3) }
        }

        postRepository.saveAll(posts)

        // Act
        val post = postService.getById(posts[0].id)

        // Assert
        val postFromRepo = postRepository.findByIdOrNull(posts[0].id)
        assertThat(postFromRepo?.mapToOutDTO()).isEqualTo(post)
    }

    @Test
    fun `test getById, if post not found`() {
        // Arrange
        val posts = posts {
            that { hasCount(3) }
        }
        postRepository.saveAll(posts)

        // Act & Assert
        assertThatThrownBy { postService.getById(ObjectId.get().toString()) }
            .isInstanceOf(PostNotFoundException::class.java)
    }

    @Test
    fun `test create`() {
        // Act
        val post = postService.create(CreateUpdatePostDTO(generateRandomTextWithLength(MAX_POST_TEXT_LENGTH)))

        // Assert
        val allPosts = postRepository.findAll()
        assertThat(allPosts).hasSize(1)

        val postFromRepo = postRepository.findByIdOrNull(post.id)
        assertThat(postFromRepo?.text).isEqualTo(post.text)
    }

    @Test
    fun `test create, if text is too long`() {
        assertThatThrownBy {
            postService.create(CreateUpdatePostDTO(generateRandomTextWithLength(MAX_POST_TEXT_LENGTH + 1)))
        }.isInstanceOf(PostTextIsTooLongException::class.java)
    }

    @Test
    fun `test delete`() {
        // Arrange
        val posts = posts {
            that {
                hasCount(3)
            }
        }
        val deletedPost = posts[0]
        postRepository.saveAll(posts)

        // Act
        val postFromService = postService.delete(deletedPost.id)

        // Assert
        assertThat(deletedPost.id.toString()).isEqualTo(postFromService.id)

        val actualCount = postRepository.count()
        assertThat(actualCount).isEqualTo(2)
    }

    @Test
    fun `test delete, if post not found`() {
        // Arrange
        val posts = posts {
            that { hasCount(3) }
        }
        postRepository.saveAll(posts)

        // Act & Assert
        assertThatThrownBy { postService.delete(ObjectId.get().toString()) }
            .isInstanceOf(PostNotFoundException::class.java)
    }
}
